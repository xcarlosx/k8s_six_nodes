#!/bin/bash

vagrant up
vagrant halt
vagrant package --output kuberbox.box
vagrant box add kuberbox kuberbox.box --force
vagrant destroy -f
