#!/usr/bin/env bash

DEBUG=true

## FIX FIX FIX
# Eliminamos auto update
cp /usr/lib/apt/apt.systemd.daily{,.orig}
echo "" > /usr/lib/apt/apt.systemd.daily

function services_disabled {
  for xx in disable stop; do
    systemctl ${xx} cloud-init-local.service cloud-init.service cloud-final.service cloud-config.service
    systemctl ${xx} apparmor.service
    systemctl ${xx} lxd.socket lxd-containers.service
    systemctl ${xx} lvm2-monitor.service
    systemctl ${xx} networkd-dispatcher.service
    systemctl ${xx} accounts-daemon.service
    systemctl ${xx} snapd.service snapd.socket snap-core-4917.mount snap-core-5662.mount snapd.seeded.service
    systemctl ${xx} polkit.service
  done
  update-rc.d unattended-upgrades disable
  update-rc.d apport disable
  update-rc.d lvm2-lvmetad disable
  update-rc.d lvm2-lvmpolld disable
  update-rc.d lxcfs disable
  update-rc.d lxd disable
  update-rc.d open-vm-tools disable
  update-rc.d plymouth disable
  update-rc.d rsync disable
  update-rc.d lvm2 disable
  apt-get -y remove --purge cloud-guest-utils cloud-init cloud-initramfs-copymods cloud-initramfs-dyn-netconf
  apt-get -y remove --purge cryptsetup cryptsetup-bin
  apt-get -y remove --purge lxcfs lxd lxd-client
  apt-get -y remove --purge xauth
  apt-get -y remove --purge ureadahead
  #Desactivamos la autoactualizacion
  systemctl disable apt-daily.timer
  systemctl disable apt-daily-upgrade.timer
}

function system_update {
  if [ "$DEBUG" = "true" ]; then echo "Actualizando el sistema operativo."; fi
  ex +"%s@DPkg@//DPkg" -cwq /etc/apt/apt.conf.d/70debconf
  dpkg-reconfigure debconf -f noninteractive -p critical
  DEBIAN_FRONTEND="noninteractive"
  DEBIAN_PRIORITY="critical"
  apt-get update -yq
  apt-get -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" dist-upgrade
  if [ "$DEBUG" = "true" ]; then echo "Eliminamos swap."; fi
  swapoff -a
  sed -i.bak '/ swap / s/^/#/' /etc/fstab
  echo "
*    soft nofile 1048576
*    hard nofile 1048576
root soft nofile 1048576
root hard nofile 1048576" >> /etc/security/limits.conf
}

function docker_install {
  if [ "$DEBUG" = "true" ]; then echo "Instalando Docker."; fi
  apt-get install -yq apt-transport-https ca-certificates curl gnupg2 software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  apt-get update -yq && apt-get -yq install docker-ce=5:18.09.9~3-0~ubuntu-bionic
  apt-mark hold docker-ce
}

function kubernetes_install {
  if [ "$DEBUG" = "true" ]; then echo "Instalando Kubernetes."; fi
  curl --silent https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
  #apt-get update -yq && apt-get install -qy kubelet=1.15.5-00 kubeadm=1.15.5-00 kubectl=1.15.5-00
  apt-get update -yq && apt-get install -qy kubelet kubeadm kubectl
  apt-mark hold kubelet kubeadm kubectl
  mkdir -p  /var/lib/kubelet/
  touch  /var/lib/kubelet/config.yaml
}

services_disabled
system_update
docker_install
kubernetes_install

apt-get clean
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
cat /dev/null > ~/.bash_history && history -c && exit
