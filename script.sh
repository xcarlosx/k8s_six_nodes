#!/bin/bash -x

# RED EXTERNA: 10.90.90
# RED INTERNA: 172.16.1
# POD: 192.168

#Debug true or false
DEBUG="true"

KUBE01="172.16.1.11"
KUBE02="172.16.1.12"
KUBE03="172.16.1.13"
KUBE04="172.16.1.14"
KUBE05="172.16.1.15"
KUBE06="172.16.1.16"
KUBE07="172.16.1.17"
KUBE08="172.16.1.18"
KUBE09="172.16.1.19"
KUBEVIP="172.16.1.10"
KUBEVIPEXT="10.90.90.170"
#KUBEMK KUBEMASTERKING KUBEMS KUBEMASTERNOKING
KUBEMK=${KUBE04}
KUBEMS="${KUBE05} ${KUBE06}"
KUBEETCD1=${KUBE01}
KUBEETCD2=${KUBE02}
KUBEETCD3=${KUBE03}

PATHCERTS="/data/certs"
PATHKUBECONFIG="/data/config"

# FIX FIX FIX
echo 'SUBSYSTEM=="net", ACTION=="add|change", ENV{INTERFACE}=="br-*", PROGRAM="/bin/sleep 0.5"
SUBSYSTEM=="net", ACTION=="add|change", ENV{INTERFACE}=="docker[0-9]*", PROGRAM="/bin/sleep 0.5"' > /etc/udev/rules.d/01-net-setup-link.rules
systemctl restart udev
# FIX FIX FIX
# He creado un interfaz "publico" porque jugar con puertos haciendo el proxy de vbox solo complica las cosas y no es real.
# El interfaz proxy que genera siempre tiene de interfaz enp0s3
if [ "$HOSTNAME" = "kube01" ] || [ "$HOSTNAME" = "kube04" ] || [ "$HOSTNAME" = "kube05" ] || [ "$HOSTNAME" = "kube06" ]; then
  if [ "$DEBUG" = "true" ]; then echo "Aplicando fix de red"; fi
  route add default gw 10.90.90.254
  eval `route -n | awk '{ if ($8 =="enp0s3" && $2 != "0.0.0.0") print "route del default gw " $2; }'`
  if [ "$DEBUG" = "true" ]; then route -n; fi
fi
## FIX FIX FIX
# Eliminamos auto update
cp /usr/lib/apt/apt.systemd.daily{,.orig}
echo "" > /usr/lib/apt/apt.systemd.daily

function etc_hosts_fix {
  if [ "$DEBUG" = "true" ]; then echo "Modificando fichero de hosts."; fi
  sed -i.bak '/^127.*kube.*/d' /etc/hosts
  echo "#Kubemasters
${KUBE01}  kube01 kubeetcd01
${KUBE02}  kube02 kubeetcd02
${KUBE03}  kube03 kubeetcd03
${KUBE04}  kube04 kubemaster01
${KUBE05}  kube05 kubemaster02
${KUBE06}  kube06 kubemaster03
${KUBE07}  kube07 minion01
${KUBE08}  kube08 minion02
${KUBE09}  kube09 minion03" >> /etc/hosts
  if [ "$DEBUG" = "true" ]; then cat /etc/hosts; fi
}

function sshkeys_install {
  if [ "$DEBUG" = "true" ]; then echo "Copiando keys predefinidas. Cambiar en sistema de produccion."; fi
  mkdir -p /root/.ssh && chmod 600 /root/.ssh
  cat > /root/.ssh/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzFeE4K9sE2O3+5mHnvMOfj5eZCgdCsopKRw6kDTBYmlfQt0F
DUjBXGqp5H8IGfekv5EnVx3+MFN3CrvLbr0N8R0oCcuSk4mZY6aqxqq8oqXCXRoY
U0hU2cs/uvJohuFqNvaaYLdrkbyv8D2vi61OV0btBW26Xv9Z2Q6Cu+ABxt7bqxTi
ffnQNZNFZXy6dpDwiurpg2nOpoX18XUQRJzJZM+KoGPFauR4l2cVmCcYOqLegaOy
C5JaH3YDK1HmqcxWcyato4auswxnttm1OdixulSyS6AcR4c/CKVgEFXfEoO/83/p
JFW1BaVOzgRGxv8Qe57Kvwf6wQT+ylowCyLV+QIDAQABAoIBAQCsrDgo9ZPKPrW1
8mLcOl/A5ks1L4B7IaHuj2Adg7b2xjwtcD9YnmZnBetSntXlu5KIHsyznTCy/ZTW
iSK7DR6+5Ph0boYKDniD8M7W2mhUuOWpbuenWLP65vmPrFAPymPK1lBV8ikAGgZC
aJ3y1hwhPFoGAFdA916hQNXQlNZriVwZ2bbWbkyn8MY7fFVPOhKSCiKZxPSJis4A
B40+EtplVP/r42C7+xwjM9s0tsz8XeSsH10m/wmFbG+x9iTBv60bkdqd/BgvN+bi
9HV31KGnhz8FbfPREwMqAK9hjme1I8tFynFyNgaczZodQzCewtQ3iQbtgWMz9rx0
uh34mXZBAoGBAO2xeRKRm6203R7CKKguBH7I0OEEjWG15yHqMTjJwmENnLCce8et
FJhXIEdcXWGmoXLLgV9gjrZLadyNq+JdN2Dkfb7x4vjwo7n8LblSfm6B37RvENmn
rXYf6ywJtCZMfoGqAN8UG1w++LetiwzjFtRYxVJYb71s8iZpXMlPdxjNAoGBANwU
d4MmfxIJM4ZE8P77PjP7LarEm+basZ3bV9LxNb+FqvpoCP5kPr56E+PrkTLWcmR4
7jq/vbmp0yPB2CG8hVvLWawcURWw4SvIopbCfdAUc7Q6w4uhDtnqZGa3X5zWKjjm
KGDRRTvcyamMbuGboXCVIhIjEPEayAM0hubzoCHdAoGBANAueJRC488oceLWuzVR
2K4SxJ/QlOPXpjCWJbZPP+gxO+OTqvYgKcGjzamyxDca8pzQkOcwcs5HDtkVXE5O
xooeleP/h4Pxf/UNYh7GmeW/6mbmQCPiMJMdMaTIevhwVvzKISlBOxV4Me7uGLOY
0g36EhklS/jkytHJxnKrHuyhAoGAV/O5+2jUGZIhzEQbB9vCdSWpj3h5UZ4Oo6Z4
B64sK7kwplgTWA8Z4CeLcWfAJE/9cVe+5S2hleRfpAeg54KezRuMX7MFWKTSqBvL
lnJts02pG9rsMQ4RkFcS/tXD2TrhrtGUS3T2tUC8Ow0LdxAqSYLgFd17tM9eJaHF
U+EDGuUCgYB/4kWcCnlUadB73Rlf1LXnEWXn5qE+932ey18CxU7DR06uGm23OBlH
XeQfIB5vVMulZJjcF3+gXuU+MLYyrpvin084g6Y4BnoRcdUgFEKVi3vA8ji/7gYb
Yiz6rLTvJcmPf8EgCYERZtuu7Mo1jB0pykLWSl4M7LWSU/lu9ywBfQ==
-----END RSA PRIVATE KEY-----
EOF
  cat > /root/.ssh/id_rsa.pub <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMV4Tgr2wTY7f7mYee8w5+Pl5kKB0KyikpHDqQNMFiaV9C3QUNSMFcaqnkfwgZ96S/kSdXHf4wU3cKu8tuvQ3xHSgJy5KTiZljpqrGqryipcJdGhhTSFTZyz+68miG4Wo29ppgt2uRvK/wPa+LrU5XRu0Fbbpe/1nZDoK74AHG3turFOJ9+dA1k0VlfLp2kPCK6umDac6mhfXxdRBEnMlkz4qgY8Vq5HiXZxWYJxg6ot6Bo7ILklofdgMrUeapzFZzJq2jhq6zDGe22bU52LG6VLJLoBxHhz8IpWAQVd8Sg7/zf+kkVbUFpU7OBEbG/xB7nsq/B/rBBP7KWjALItX5 root@kubernetes
EOF
  cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
  chmod 640 /root/.ssh/id_rsa.pub && chmod 600 /root/.ssh/id_rsa /root/.ssh/authorized_keys
  sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
  service ssh restart
  if [ "$DEBUG" = "true" ]; then ls -la /root/.ssh/; systemctl status ssh; fi
}

function sysctl_set {
  if [ "$DEBUG" = "true" ]; then echo "Optimizando el sistema. Sysctl."; fi
  echo "fs.file-max = 2097152" >> /etc/sysctl.conf
  echo "fs.nr_open = 1048576" >> /etc/sysctl.conf
  echo "net.ipv4.netfilter.ip_conntrack_max = 1048576" >> /etc/sysctl.conf
  echo "net.nf_conntrack_max = 1048576" >> /etc/sysctl.conf
  echo "net.core.somaxconn = 1048576" >> /etc/sysctl.conf
  #echo "net.bridge.bridge-nf-call-ip6tables = 1" >> /etc/sysctl.conf
  #echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.conf
  sysctl -p
}

function haproxy_hearthbeat_deb {
  dpkg-reconfigure debconf -f noninteractive -p critical
  DEBIAN_FRONTEND="noninteractive"
  DEBIAN_PRIORITY="critical"
  apt-get -q update
  apt-get -yq  install haproxy hatop heartbeat crmsh
}

function haproxy_install {
  if [ "$DEBUG" = "true" ]; then echo "Instalando y configurando HAProxy"; fi
  mkdir -p /etc/haproxy
  mv /etc/haproxy/haproxy.cfg{,.back}
  cat > /etc/haproxy/haproxy.cfg <<EOF
#hatop --unix-socket=/var/run/haproxy/haproxy.sock
global
  user haproxy
  group haproxy
  stats socket /run/haproxy/haproxy.sock mode 660 level admin

defaults
  mode http
  log global
  retries 2
  #timeout connect 3000ms
  #timeout server 5000ms
  #timeout client 5000ms
  timeout connect 60000ms
  timeout server 60000ms
  timeout client 60000ms

listen stats
  bind *:8080
  mode http
  stats enable
  stats uri /
  stats refresh 5s

frontend kubernetes
  bind ${KUBEVIP}:6443
  option tcplog
  mode tcp
  default_backend kubernetes-master-nodes

backend kubernetes-master-nodes
  mode tcp
  balance roundrobin
  option tcp-check
  server k8s-master-1 ${KUBE04}:6443 check fall 3 rise 2
  server k8s-master-2 ${KUBE05}:6443 check fall 3 rise 2
  server k8s-master-3 ${KUBE06}:6443 check fall 3 rise 2
EOF
  #To allow system services binding on the non-local IP
  echo "net.ipv4.ip_nonlocal_bind=1" >> /etc/sysctl.conf
  sysctl -p
  sleep 2
  systemctl enable haproxy
  systemctl restart haproxy
  if [ "$DEBUG" = "true" ]; then
    systemctl status haproxy
    netstat -ntlp
  fi
}

function heartbeat_install {
#   systemctl enable heartbeat
#   # echo -n enunlugardelamancha | md5sum
#   # a7a96bda115627dae2a903d95cb35e1c
#   mkdir -p /etc/ha.d/
#   cat > /etc/ha.d/authkeys <<EOF
# auth 1
# 1 md5 a7a96bda115627dae2a903d95cb35e1c
# EOF
#   chmod 600 /etc/ha.d/authkeys
#   if [ "${MIMAINIP}" = "${KUBEETCD1}" ]; then
#     cat > /etc/ha.d/ha.cf <<EOF
# #       keepalive: how many seconds between heartbeats
# #
# keepalive 2
# #
# #       deadtime: seconds-to-declare-host-dead
# #
# deadtime 10
# #
# #       What UDP port to use for udp or ppp-udp communication?
# #
# udpport        694
# bcast enp0s8
# mcast enp0s8 225.0.0.1 694 1 0
# ucast enp0s8 ${KUBEETCD2}
# ucast enp0s8 ${KUBEETCD3}
# #       What interfaces to heartbeat over?
# udp     enp0s8
# #
# #       Facility to use for syslog()/logger (alternative to log/debugfile)
# #
# logfacility     local0
# #
# #       Tell what machines are in the cluster
# #       node    nodename ...    -- must match uname -n
# node    kube01
# node    kube02
# node    kube03
# EOF
#   elif [ "${MIMAINIP}" = "${KUBEETCD2}" ]; then
#     cat > /etc/ha.d/ha.cf <<EOF
# #       keepalive: how many seconds between heartbeats
# #
# keepalive 2
# #
# #       deadtime: seconds-to-declare-host-dead
# #
# deadtime 10
# #
# #       What UDP port to use for udp or ppp-udp communication?
# #
# udpport        694
# bcast  enp0s8
# mcast enp0s8 225.0.0.1 694 1 0
# ucast enp0s8 ${KUBEETCD1}
# ucast enp0s8 ${KUBEETCD3}
# #       What interfaces to heartbeat over?
# udp     enp0s8
# #
# #       Facility to use for syslog()/logger (alternative to log/debugfile)
# #
# logfacility     local0
# #
# #       Tell what machines are in the cluster
# #       node    nodename ...    -- must match uname -n
# node    kube01
# node    kube02
# node    kube03
# EOF
#   elif [ "${MIMAINIP}" = "${KUBEETCD3}" ]; then
#     cat > /etc/ha.d/ha.cf <<EOF
# #       keepalive: how many seconds between heartbeats
# #
# keepalive 2
# #
# #       deadtime: seconds-to-declare-host-dead
# #
# deadtime 10
# #
# #       What UDP port to use for udp or ppp-udp communication?
# #
# udpport        694
# bcast  enp0s8
# mcast enp0s8 225.0.0.1 694 1 0
# ucast enp0s8 ${KUBEETCD1}
# ucast enp0s8 ${KUBEETCD2}
# #       What interfaces to heartbeat over?
# udp     enp0s8
# #
# #       Facility to use for syslog()/logger (alternative to log/debugfile)
# #
# logfacility     local0
# #
# #       Tell what machines are in the cluster
# #       node    nodename ...    -- must match uname -n
# node    kube01
# node    kube02
# node    kube03
# EOF
#   else
#     echo "No es un servidor de haproxy."
#     exit 1
#   fi
#   cat > /etc/ha.d/haresources <<EOF
# kube01  ${KUBEVIP}
# EOF
#   systemctl restart heartbeat
#   if [ "$DEBUG" = "true" ]; then
#     systemctl status heartbeat
#     if [ "${MIMAINIP}" = "${KUBEETCD3}" ]; then
#       nc -v ${KUBEVIP} 6443
#     fi
#   fi
  mkdir -p /etc/corosync/service.d/
  if [ "${MIMAINIP}" = "${KUBEETCD1}" ]; then
    #Escribe /etc/corosync/authkey
    corosync-keygen -l
    cat > /etc/corosync/corosync.conf <<EOF
totem {
  version: 2
  cluster_name: lbcluster
  transport: udpu
  interface {
    ringnumber: 0
    bindnetaddr: ${KUBEETCD1}
    broadcast: yes
    mcastport: 5405
  }
}

quorum {
  provider: corosync_votequorum
}

nodelist {
  node {
    ring0_addr: ${KUBEETCD1}
    name: kube01
    nodeid: 1
  }
  node {
    ring0_addr: ${KUBEETCD2}
    name: kube02
    nodeid: 2
  }
  node {
    ring0_addr: ${KUBEETCD3}
    name: kube03
    nodeid: 3
  }
}

logging {
  to_logfile: yes
  logfile: /var/log/corosync/corosync.log
  to_syslog: yes
  timestamp: on
}
EOF
  cat > /etc/corosync/service.d/pcmk <<EOF
service {
  name: pacemaker
  ver: 1
}
EOF
  elif [ "${MIMAINIP}" = "${KUBEETCD2}" ]; then
    scp -o "StrictHostKeyChecking no" ${KUBEETCD1}:/etc/corosync/authkey /etc/corosync/
    chmod 400 /etc/corosync/authkey
    cat > /etc/corosync/corosync.conf <<EOF
totem {
  version: 2
  cluster_name: lbcluster
  transport: udpu
  interface {
    ringnumber: 0
    bindnetaddr: ${KUBEETCD2}
    broadcast: yes
    mcastport: 5405
  }
}

quorum {
  provider: corosync_votequorum
}

nodelist {
  node {
    ring0_addr: ${KUBEETCD1}
    name: kube01
    nodeid: 1
  }
  node {
    ring0_addr: ${KUBEETCD2}
    name: kube02
    nodeid: 2
  }
  node {
    ring0_addr: ${KUBEETCD3}
    name: kube03
    nodeid: 3
  }
}

logging {
  to_logfile: yes
  logfile: /var/log/corosync/corosync.log
  to_syslog: yes
  timestamp: on
}
EOF
  cat > /etc/corosync/service.d/pcmk <<EOF
service {
  name: pacemaker
  ver: 1
}
EOF
  elif [ "${MIMAINIP}" = "${KUBEETCD3}" ]; then
    scp -o "StrictHostKeyChecking no" ${KUBEETCD1}:/etc/corosync/authkey /etc/corosync/
    chmod 400 /etc/corosync/authkey
    cat > /etc/corosync/corosync.conf <<EOF
totem {
  version: 2
  cluster_name: lbcluster
  transport: udpu
  interface {
    ringnumber: 0
    bindnetaddr: ${KUBEETCD3}
    broadcast: yes
    mcastport: 5405
  }
}

quorum {
  provider: corosync_votequorum
}

nodelist {
  node {
    ring0_addr: ${KUBEETCD1}
    name: kube01
    nodeid: 1
  }
  node {
    ring0_addr: ${KUBEETCD2}
    name: kube02
    nodeid: 2
  }
  node {
    ring0_addr: ${KUBEETCD3}
    name: kube03
    nodeid: 3
  }
}

logging {
  to_logfile: yes
  logfile: /var/log/corosync/corosync.log
  to_syslog: yes
  timestamp: on
}
EOF
    cat > /etc/corosync/service.d/pcmk <<EOF
service {
  name: pacemaker
  ver: 1
}
EOF
  else
    echo "No es un servidor de haproxy."
    exit 1
  fi
  systemctl enable corosync
  systemctl restart corosync
  if [ "$DEBUG" = "true" ]; then
    sleep 5 && corosync-cmapctl | grep members
  fi
  update-rc.d pacemaker defaults 20 01
  systemctl restart pacemaker
  if [ "${MIMAINIP}" = "${KUBEETCD3}" ]; then
    sleep 10
    crm configure property stonith-enabled=false
    crm configure property no-quorum-policy=ignore
    crm configure primitive vip_int ocf:heartbeat:IPaddr2 params ip="${KUBEVIP}" nic="enp0s8" cidr_netmask="32" op monitor interval="10s"
    crm configure primitive vip_ext ocf:heartbeat:IPaddr2 params ip="${KUBEVIPEXT}" nic="enp0s3" cidr_netmask="32" op monitor interval="10s"
    crm configure show
  fi
}

function etcd_kubelet {
  cat << EOF > /etc/systemd/system/kubelet.service.d/20-etcd-service-manager.conf
[Service]
ExecStart=
ExecStart=/usr/bin/kubelet --address=127.0.0.1 --pod-manifest-path=/etc/kubernetes/manifests
Restart=always
EOF
  systemctl daemon-reload
  systemctl restart kubelet
  if [ "$DEBUG" = "true" ]; then systemctl status kubelet; fi
}

function etcd_certs_files {
  mkdir -p /tmp/${KUBEETCD1}/ /tmp/${KUBEETCD2}/ /tmp/${KUBEETCD3}/
  ETCDHOSTS=(${KUBEETCD1} ${KUBEETCD2} ${KUBEETCD3})
  NAMES=("kube01" "kube02" "kube03")
  for i in "${!ETCDHOSTS[@]}"; do
    HOST=${ETCDHOSTS[$i]}
    NAME=${NAMES[$i]}
    cat > /tmp/${HOST}/kubeadmcfg.yaml <<EOF
apiVersion: "kubeadm.k8s.io/v1beta1"
kind: ClusterConfiguration
etcd:
    local:
        serverCertSANs:
        - ${HOST}
        - ${ETCDHOSTS[0]}
        - ${ETCDHOSTS[1]}
        - ${ETCDHOSTS[2]}
        peerCertSANs:
        - ${HOST}
        - ${ETCDHOSTS[0]}
        - ${ETCDHOSTS[1]}
        - ${ETCDHOSTS[2]}
        extraArgs:
            initial-cluster: ${NAMES[0]}=https://${ETCDHOSTS[0]}:2380,${NAMES[1]}=https://${ETCDHOSTS[1]}:2380,${NAMES[2]}=https://${ETCDHOSTS[2]}:2380
            initial-cluster-state: new
            name: ${NAME}
            listen-peer-urls: https://${HOST}:2380
            listen-client-urls: https://${HOST}:2379
            advertise-client-urls: https://${HOST}:2379
            initial-advertise-peer-urls: https://${HOST}:2380
apiServer:
  extraArgs:
    enable-admission-plugins: AlwaysPullImages,DefaultStorageClass
EOF
  done
  kubeadm init phase certs etcd-ca
  if [ "$DEBUG" = "true" ]; then cat /tmp/${HOST}/kubeadmcfg.yaml; fi
}

function etcd_up_three_nodes {
  #Al finalizar el tercero en discordia.
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD1} 'kubeadm init phase certs etcd-server --config=/tmp/172.16.1.12/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-peer --config=/tmp/172.16.1.12/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-healthcheck-client --config=/tmp/172.16.1.12/kubeadmcfg.yaml \
  && kubeadm init phase certs apiserver-etcd-client --config=/tmp/172.16.1.12/kubeadmcfg.yaml \
  && cp -R /etc/kubernetes/pki /tmp/172.16.1.12/ \
  && find /etc/kubernetes/pki -not -name ca.crt -not -name ca.key -type f -delete \
  && find /tmp/172.16.1.12 -name ca.key -type f -delete \
  && scp -o "StrictHostKeyChecking no" -r /tmp/172.16.1.12/pki 172.16.1.12:/etc/kubernetes/ \
  && scp -o "StrictHostKeyChecking no" -r /tmp/172.16.1.12/kubeadmcfg.yaml 172.16.1.12:'
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD1} 'kubeadm init phase certs etcd-server --config=/tmp/172.16.1.13/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-peer --config=/tmp/172.16.1.13/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-healthcheck-client --config=/tmp/172.16.1.13/kubeadmcfg.yaml \
  && kubeadm init phase certs apiserver-etcd-client --config=/tmp/172.16.1.13/kubeadmcfg.yaml \
  && cp -R /etc/kubernetes/pki /tmp/172.16.1.13/ \
  && find /etc/kubernetes/pki -not -name ca.crt -not -name ca.key -type f -delete \
  && find /tmp/172.16.1.13 -name ca.key -type f -delete \
  && scp -o "StrictHostKeyChecking no" -r /tmp/172.16.1.13/pki 172.16.1.13:/etc/kubernetes/ \
  && scp -o "StrictHostKeyChecking no" -r /tmp/172.16.1.13/kubeadmcfg.yaml 172.16.1.13:'
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD1} 'kubeadm init phase certs etcd-server --config=/tmp/172.16.1.11/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-peer --config=/tmp/172.16.1.11/kubeadmcfg.yaml \
  && kubeadm init phase certs etcd-healthcheck-client --config=/tmp/172.16.1.11/kubeadmcfg.yaml \
  && kubeadm init phase certs apiserver-etcd-client --config=/tmp/172.16.1.11/kubeadmcfg.yaml \
  && cp -R /etc/kubernetes/pki /tmp/172.16.1.11/'
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD1} 'kubeadm init phase etcd local --config=/tmp/172.16.1.11/kubeadmcfg.yaml'
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD2} 'kubeadm init phase etcd local --config=/root/kubeadmcfg.yaml'
  ssh -o "StrictHostKeyChecking no" ${KUBEETCD3} 'kubeadm init phase etcd local --config=/root/kubeadmcfg.yaml'
  if [ "$DEBUG" = "true" ]; then
    sleep 5
    docker run --rm -it --net host -v /etc/kubernetes:/etc/kubernetes quay.io/coreos/etcd:v3.2.24 etcdctl --cert-file /etc/kubernetes/pki/etcd/peer.crt --key-file /etc/kubernetes/pki/etcd/peer.key --ca-file /etc/kubernetes/pki/etcd/ca.crt --endpoints https://172.16.1.11:2379 cluster-health
    #Se toma un buen rato en levantar todo... (RESULTADO:)
    #member 2cd4d60db6db4371 is healthy: got healthy result from https://172.16.1.11:2379
    #member 3f62a4d067dd828a is healthy: got healthy result from https://172.16.1.13:2379
    #member 6ac10dc079d59789 is healthy: got healthy result from https://172.16.1.12:2379
  fi
}

function kubemasterking_up {
  mkdir -p /etc/kubernetes/pki/etcd/
  scp -q -o "StrictHostKeyChecking no" ${KUBEETCD1}:/etc/kubernetes/pki/etcd/ca.crt /etc/kubernetes/pki/etcd/ca.crt
  scp -q -o "StrictHostKeyChecking no" ${KUBEETCD1}:/etc/kubernetes/pki/apiserver-etcd-client.* /etc/kubernetes/pki/
  cat > /root/kubeadm-config.yaml <<EOF
apiVersion: kubeadm.k8s.io/v1beta1
kind: ClusterConfiguration
kubernetesVersion: stable
apiServer:
  certSANs:
  - "${KUBEVIP}"
controlPlaneEndpoint: "${KUBEVIP}:6443"
etcd:
    external:
        endpoints:
        - https://${KUBEETCD1}:2379
        - https://${KUBEETCD2}:2379
        - https://${KUBEETCD3}:2379
        caFile: /etc/kubernetes/pki/etcd/ca.crt
        certFile: /etc/kubernetes/pki/apiserver-etcd-client.crt
        keyFile: /etc/kubernetes/pki/apiserver-etcd-client.key
#networking:
  #podSubnet: 192.168.0.0/16
EOF
kubeadm init --v 9 --config /root/kubeadm-config.yaml | tee -a /root/cluster_info_${KUBEMK}.txt
mkdir -p ~/.kube && cp /etc/kubernetes/admin.conf ~/.kube/config && chmod 600 ~/.kube/config
if [ "$DEBUG" = "true" ]; then kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes -o wide; fi
}

function cninetwork_install {
  kubectl --kubeconfig /etc/kubernetes/admin.conf apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
  if [ "$DEBUG" = "true" ]; then
    sleep 10
    kubectl --kubeconfig /etc/kubernetes/admin.conf get all --all-namespaces
    kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes -o wide
  fi
}

function control_plane_up {
  mkdir -p /etc/kubernetes/pki/etcd/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/ca.crt /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/ca.key /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/sa.key /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/sa.pub /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/front-proxy-ca.crt /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/front-proxy-ca.key /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/apiserver-etcd-client.crt /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/apiserver-etcd-client.key /etc/kubernetes/pki/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/pki/etcd/ca.crt /etc/kubernetes/pki/etcd/
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/etc/kubernetes/admin.conf /etc/kubernetes/
  mkdir -p ~/.kube && cp /etc/kubernetes/admin.conf ~/.kube/config && chmod 600 ~/.kube/config
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/root/cluster_info_${KUBEMK}.txt /tmp/
  JOINMASTERCOMMAND="$(cat /tmp/cluster_info_${KUBEMK}.txt | tail -8 | head -3 | sed 's/\\//g' | sed ':a;N;$!ba;s/\n/ /g')"
  $JOINMASTERCOMMAND | tee -a /root/cluster_info_${MIMAINIP}.txt
}

function minions_up {
  scp -q  -o "StrictHostKeyChecking no" ${KUBEMK}:/root/cluster_info_${KUBEMK}.txt /tmp/
  JOINCOMMAND="$(tail -2 /tmp/cluster_info_${KUBEMK}.txt | sed 's/\\//'| sed ':a;N;$!ba;s/\n/ /g')"
  $JOINCOMMAND | tee -a /root/worker_info_${MIMAINIP}.txt
}

#
# COMIENZO: haproxy + cluster etcd
#
apt-get update
systemctl stop apt-daily.service
systemctl kill --kill-who=all apt-daily.service
while ! (systemctl list-units --all apt-daily.service | egrep -q '(dead|failed)'); do
  sleep 1;
done
etc_hosts_fix
sshkeys_install
sysctl_set
apt-get -qy install nfs-kernel-server
MIMAINIP=$(grep ${HOSTNAME} /etc/hosts|awk '{print $1}'|tail -1)
#MIMAINIP=$(host ${HOSTNAME}|awk '{print $4}')
if [ "$DEBUG" = "true" ]; then echo "SERVIDOR: ${MIMAINIP}"; fi
if [ "$HOSTNAME" = "kube01" ]; then
  etcd_certs_files
fi
if [ "$HOSTNAME" = "kube01" ] || [ "$HOSTNAME" = "kube02" ] || [ "$HOSTNAME" = "kube03" ]; then
  haproxy_hearthbeat_deb
  haproxy_install
  heartbeat_install
  etcd_kubelet
  if [ "$HOSTNAME" = "kube03" ]; then
    etcd_up_three_nodes
  fi
fi
#
# FASE 2: Kubemasters + weave
#
if [ "$HOSTNAME" = "kube04" ]; then
  kubemasterking_up
  cninetwork_install
fi
if [ "$HOSTNAME" = "kube05" ] || [ "$HOSTNAME" = "kube06" ]; then
  control_plane_up
fi
#
# FASE 3: Minions
#
if [ "$HOSTNAME" = "kube07" ] || [ "$HOSTNAME" = "kube08" ] || [ "$HOSTNAME" = "kube09" ]; then
  minions_up
  sleep 5
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'kubectl get nodes'
fi
#
# FASE 4: AddOns
#
if [ "$HOSTNAME" = "kube09" ]; then
  #Despues de que esta todo instalado y funcionando.
  for xx in ${KUBEMK} ${KUBEMS}; do
    ssh -o "StrictHostKeyChecking no" ${xx} "curl --silent https://get.helm.sh/helm-v2.15.2-linux-amd64.tar.gz -o /tmp/helm-v2.15.2-linux-amd64.tar.gz \
      && tar xzf /tmp/helm-v2.15.2-linux-amd64.tar.gz -C /tmp \
      && cp /tmp/linux-amd64/helm /tmp/linux-amd64/tiller /usr/local/bin"
    if [ "${KUBEMK}" = "${xx}" ]; then
      ssh -o "StrictHostKeyChecking no" ${xx} "kubectl -n kube-system create serviceaccount tiller \
        && kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller"
      ssh -o "StrictHostKeyChecking no" ${xx} "helm init --service-account=tiller --history-max=300 \
        && helm version"
      sleep 10
    else
      ssh -o "StrictHostKeyChecking no" ${xx} "helm init --service-account=tiller --history-max=300 -c; \
        helm version"
    fi
  done
  #
  # helm diff: compara para upgrades, etc..
  #
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm plugin install https://github.com/databus23/helm-diff --version master'
  #
  # ingress-nginx
  #
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm install stable/nginx-ingress --name nginx-ingress --namespace kube-system \
    --set rbac.create=true \
    --set controller.kind=DaemonSet \
    --set controller.service.type=ClusterIP \
    --set controller.service.externalIPs[0]=10.90.90.174 \
    --set controller.service.externalIPs[1]=10.90.90.175 \
    --set controller.service.externalIPs[2]=10.90.90.176'
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'kubectl create namespace monitoring'
  #
  # Prometheus + grafana + alertmanager + state-metrics
  #
  #Hay que cambiar la configuracion para volumenes persistentes
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm install stable/prometheus-operator \
    --name prometheus-operator \
    --namespace monitoring \
    --set grafana.adminPassword=password \
    --set prometheus-node-exporter.service.port=30206 \
    --set prometheus-node-exporter.service.targetPort=30206 \
    --set grafana.defaultDashboardsEnabled=true \
    --set grafana.ingress.enabled=true \
    --set grafana.ingress.hosts[0]="grafana.mykube.local"'
  #
  # NFS-SERVER-PROVISIONER
  # (En el "DEMO" uso el nodo9 para que tenga un nfs y pueda montar pv y pvc)
  /sbin/parted -a optimal /dev/sda ---pretend-input-tty resizepart 1 Yes 200GB
  resize2fs /dev/sda1
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm fetch -d /tmp --untar stable/nfs-server-provisioner; \
    sed -i "s/apps\/v1beta2/apps\/v1/g" /tmp/nfs-server-provisioner/templates/statefulset.yaml; \
    helm install /tmp/nfs-server-provisioner/ \
      --name nfs-server-provisioner \
      --namespace kube-system \
      --set storageClass.defaultClass=true \
      --set nodeSelector."kubernetes\.io/hostname"=kube09'
  #
  # HOSTPATH-provisioner
  #
  #ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm repo add rimusz https://charts.rimusz.net'
  #ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm install rimusz/hostpath-provisioner --name hostpath-provisioner --namespace kube-system
  #
  # ELK (Version de Carlos)
  #
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm repo add elastic https://helm.elastic.co'
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm fetch -d /tmp --untar elastic/elasticsearch; \
    sed -i "s/apps\/v1beta2/apps\/v1/g" /tmp/elasticsearch/templates/statefulset.yaml; \
    helm install /tmp/elasticsearch \
      --name elasticsearch \
      --namespace monitoring \
      --set volumeClaimTemplate.storageClassName=nfs \
      --set rbac.create=true \
      --set cluster.xpackEnable=true'
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm fetch -d /tmp --untar elastic/filebeat; \
    helm install /tmp/filebeat \
      --name filebeat \
      --namespace monitoring'
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm fetch -d /tmp --untar elastic/metricbeat; \
    helm install /tmp/metricbeat \
      --name metricbeat \
      --namespace monitoring'
  ssh -o "StrictHostKeyChecking no" ${KUBEMK} 'helm fetch -d /tmp --untar elastic/kibana; \
    helm install /tmp/kibana \
      --name kibana \
      --namespace monitoring \
      --set ingress.enabled=true \
      --set ingress.hosts[0]="kibana.mykube.local" \
      --set ingress.annotations."kubernetes\.io/ingress\.class"=nginx'
fi

apt-get clean
