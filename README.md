# Bienvenidos a k8s en 6 nodos
## Preambulo
Escribiendolo rapido, que ya lo retocare.
- 3 nodos con HAPROXY controlados por hearthbeat que "por ahora" exportan el api (6443), con ETCD externo, y cada uno de ellos en cluster con los otros dos.
- 3 nodos de kubemaster (al que llamo king y los control-plane).
- 3 minions
La idea es en el futuro poder usar 3 servidores fisicos, y que cada uno tenga una de estas vms (haproxy,master, minion), de forma que esta en HA real y que puede fallar cualquier maquina para que siga todo funcionando.
## Uso
Para agilizar todo hay dos pasos.
Primero generar la vmbox que usaran los 6 nodos, entrando en el directorio vmbox y ejecutando el script: create_vagrant_box.sh .
Una vez que tienes la vmbox creada y cargada como imagen vagrant, te vas a la raiz y usas la magia del vagrant: vagrant up
Para que sea un ejemplo lo mas real posible, he añadido dos interfaces:
- Una externa para poder usar los puertos de los diferentes nodos o servicios exportados (10.90.90.0).
- Otra interna para la comunicacion entre ellos (172.16.1.0).
